package de.nurmarvin.discordbot;

import de.nurmarvin.discordbot.utils.enums.SpecialChannelPermissions;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;

@Data @AllArgsConstructor
public final class Config {

    /**
     * General
     */

    private String token;
    private String sentryDsn;
    private String prefix;
    private Long logChannelId;
    private Long debugLogChannelId;
    private Long reportChannelId;
    private Long specialCategoryId;
    private Long colorPickChannelId;
    private Long privateVoiceChannelExplanationChannelId;
    private Long specialRoleChannelId;
    private Long publicVoiceChannelCategoryId;
    private Long memberCountChannelId;

    /**
     * Permissions
     */

    private ArrayList<Long> adminRoles;
    private ArrayList<Long> modRoles;
    private Boolean adminBypass;

    /**
     * Special Channel Handling (e.g. only post messages that have an image attached)
     */

    private HashMap<Long, ArrayList<SpecialChannelPermissions>> specialChannelPermissions;

    /**
     * Verification
     */

    private Long verificationMessageId;
    private Long verifiedRoleId;

    /**
     * Private Voice Channels
     */

    private Long privateChannelId;
    private Long privateChannelTemplateId;
    private Long privateChannelCategoryId;

    /**
     * Level Role IDs
     */

    private Long levelFiveRoleId;
    private Long levelTenRoleId;
    private Long levelTwentyRoleId;
    private Long levelThirtyRoleId;
    private Long levelFortyRoleId;

    /**
     * Welcomer Squad
     */

    private Long generalChannelId;
    private Long welcomerSquadRoleId;

}
