package de.nurmarvin.discordbot.handler;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.UtilTime;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class VerificationHandler extends ListenerAdapter {
    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        if (event.getMessageIdLong() == ThePalaceBot.getInstance().getConfig().getVerificationMessageId()) {
            event.getReaction().removeReaction(event.getUser()).queue();
            if (event.getReactionEmote().getName().equalsIgnoreCase("✅")) {
                event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                       event.getGuild().getRoleById(
                                                                               ThePalaceBot.getInstance().getConfig()
                                                                                           .getVerifiedRoleId()))
                     .queue();

                ServerStatsHandler.updateMemberCount(event.getGuild());

                EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

                embedBuilder.setTitle("Welcome");

                embedBuilder.appendDescription(
                        String.format("%s has just verified and is now part of our community.\n",
                                      event.getMember().getAsMention()));
                embedBuilder.appendDescription(String.format("Let's give them a warm and nice welcome! %s",
                                                             event.getGuild().getEmoteById(539352192361037834L)));
                embedBuilder.setImage("https://axolotl.club/7wZzfkPp.gif?key=HailTheAxolotl6iKMJB");
                TextChannel generalChat =
                        event.getGuild()
                             .getTextChannelById(ThePalaceBot.getInstance().getConfig().getGeneralChannelId());

                Role welcomerSquad =
                        event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getWelcomerSquadRoleId());

                final long start = System.currentTimeMillis();

                welcomerSquad.getManager().setMentionable(true)
                             .queue(success -> {
                                 final long mentionable = System.currentTimeMillis();
                                 generalChat.sendMessage(welcomerSquad.getAsMention())
                                            .queue(m -> {
                                                final long pingMessageSent = System.currentTimeMillis();
                                                welcomerSquad.getManager().setMentionable(false)
                                                             .queue(done -> {
                                                                 final long unmentionable =
                                                                         System.currentTimeMillis();
                                                                 generalChat.sendMessage(embedBuilder.build())
                                                                            .queue(messageSent -> {
                                                                                final long end =
                                                                                        System.currentTimeMillis();

                                                                                EmbedBuilder debugMessage =
                                                                                        ThePalaceBot.getInstance()
                                                                                                    .getDebugLogBase();

                                                                                debugMessage.addField("Action",
                                                                                                      "Welcomer Squad" +
                                                                                                      " Ping",
                                                                                                      false);
                                                                                debugMessage.addField(
                                                                                        "Time till mentionable",
                                                                                        UtilTime.convertSmallString(
                                                                                                mentionable - start, 0,
                                                                                                UtilTime.TimeUnit.MILLISECONDS),
                                                                                        false);
                                                                                debugMessage.addField(
                                                                                        "Time till ping message " +
                                                                                        "sent",
                                                                                        UtilTime.convertSmallString(
                                                                                                pingMessageSent - start,
                                                                                                0,
                                                                                                UtilTime.TimeUnit.MILLISECONDS),
                                                                                        false);
                                                                                debugMessage.addField(
                                                                                        "Time till unmentionable",
                                                                                        UtilTime.convertSmallString(
                                                                                                unmentionable - start,
                                                                                                0,
                                                                                                UtilTime.TimeUnit.MILLISECONDS),
                                                                                        false);
                                                                                debugMessage.addField(
                                                                                        "Mentionable Duration",
                                                                                        UtilTime.convertSmallString(
                                                                                                unmentionable - mentionable,
                                                                                                0,
                                                                                                UtilTime.TimeUnit.MILLISECONDS),
                                                                                        false);
                                                                                debugMessage.addField("Total time",
                                                                                                      UtilTime.convertSmallString(
                                                                                                              end -
                                                                                                              start, 0,
                                                                                                              UtilTime.TimeUnit.MILLISECONDS),
                                                                                                      false);

                                                                                ThePalaceBot.getInstance()
                                                                                            .debugLog(event.getGuild(),
                                                                                                      debugMessage
                                                                                                              .build());
                                                                            }, Sentry::capture);
                                                             }, Sentry::capture);
                                            }, Sentry::capture);
                             }, Sentry::capture);
            }
        }
    }
}
