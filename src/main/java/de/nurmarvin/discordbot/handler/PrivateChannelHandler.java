package de.nurmarvin.discordbot.handler;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.PrivateChannel;
import de.nurmarvin.discordbot.utils.VoiceChannelUtils;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.channel.voice.update.VoiceChannelUpdatePermissionsEvent;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PrivateChannelHandler extends ListenerAdapter {
    private final File privateChannelsFile;
    private HashMap<Long, PrivateChannel> privateChannels;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public PrivateChannelHandler() throws IOException {
        this.privateChannelsFile = new File("privateChannels.json");
        if (this.privateChannelsFile.exists())
            this.load();
        else {
            this.privateChannels = Maps.newHashMap();
            this.save();
        }
    }

    @Override
    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        if (event.getChannelJoined().getIdLong() == ThePalaceBot.getInstance().getConfig().getPrivateChannelId()) {
            this.getOrCreatePrivateChannel(event.getMember());
        }
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        this.clearEmptyChannels(event.getChannelLeft());
        if(!event.getVoiceState().inVoiceChannel()) return;
        if (event.getVoiceState().getChannel().getIdLong() == ThePalaceBot.getInstance().getConfig().getPrivateChannelId()) {
            this.getOrCreatePrivateChannel(event.getMember());
        }
    }

    @Override
    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        this.clearEmptyChannels(event.getChannelLeft());
    }

    private void clearEmptyChannels(VoiceChannel voiceChannel) {
        if(voiceChannel.getIdLong() == ThePalaceBot.getInstance().getConfig().getPrivateChannelId()) return;
        if(voiceChannel.getParent() != null) {
            if(voiceChannel.getParent().getIdLong() == ThePalaceBot.getInstance().getConfig().getPrivateChannelCategoryId()) {
                String name = voiceChannel.getName();

                if(voiceChannel.getMembers().size() > 0) return;

                scheduler.schedule(() -> {
                    if(voiceChannel.getMembers().size() == 0)
                        voiceChannel.delete().queue(success -> {
                            EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getLogBase().setColor(0xFF00a9fb);

                            embedBuilder.addField("Action", "Deleted inactive private channel", false);
                            embedBuilder.addField("Channel Name", name, false);

                            ThePalaceBot.getInstance().debugLog(voiceChannel.getGuild(), embedBuilder.build());
                        }, Sentry::capture);
                },30, TimeUnit.SECONDS);
            }
        }
    }

    @Override
    public void onVoiceChannelUpdatePermissions(VoiceChannelUpdatePermissionsEvent event) {
        if(event.getChannel().getParent() != null && event.getChannel().getParent().getIdLong() == ThePalaceBot.getInstance().getConfig().getPrivateChannelCategoryId())

            for (Member changedMember : event.getChangedMembers()) {
                event.getChannel().getPermissionOverride(changedMember).getManager().clear(Permission.CREATE_INSTANT_INVITE,
                                                                                           Permission.MANAGE_WEBHOOKS,
                                                                                           Permission.VOICE_MOVE_OTHERS,
                                                                                           Permission.VOICE_MUTE_OTHERS,
                                                                                           Permission.VOICE_DEAF_OTHERS).queue();
            }

        for (Role roles : event.getChangedRoles()) {
            event.getChannel().getPermissionOverride(roles).getManager().clear(Permission.CREATE_INSTANT_INVITE,
                                                                               Permission.MANAGE_WEBHOOKS,
                                                                               Permission.VOICE_MOVE_OTHERS,
                                                                               Permission.VOICE_MUTE_OTHERS,
                                                                               Permission.VOICE_DEAF_OTHERS).queue();
        }
    }

    private void getOrCreatePrivateChannel(Member member) {
        if (this.privateChannels.containsKey(member.getUser().getIdLong())) {
            PrivateChannel privateChannel = this.privateChannels.get(member.getUser().getIdLong());

            if(member.getGuild().getVoiceChannelById(privateChannel.getChannelId()) == null) {
                this.setupPrivateChannel(member);
                return;
            }

            member.getGuild().getController().moveVoiceMember(
                    member, member.getGuild().getVoiceChannelById(
                            this.privateChannels.get(member.getUser().getIdLong()).getChannelId())).queue();
            return;
        }

        this.setupPrivateChannel(member);
    }

    public boolean hasPrivateChannel(long userId) {
        return this.privateChannels.containsKey(userId);
    }

    public PrivateChannel getPrivatChannelByChannelId(long channelId) {
        for (PrivateChannel privateChannel : this.privateChannels.values()) {
            if(privateChannel.getChannelId().equals(channelId)) return privateChannel;
        }
        return null;
    }

    private void setupPrivateChannel(Member member) {
        member.getGuild().getController().createCopyOfChannel(member.getGuild().getVoiceChannelById(
                ThePalaceBot.getInstance().getConfig().getPrivateChannelTemplateId()))
              .setName(String.format(
                      "%s's Channel", member.getUser().getName())).setParent(
                member.getGuild().getCategoryById(ThePalaceBot.getInstance().getConfig().getPrivateChannelCategoryId())).queue(c -> {


            PrivateChannel privateChannel = this.privateChannels.get(member.getUser().getIdLong());

            if(privateChannel == null) {
                privateChannel = new PrivateChannel();
                privateChannel.setOwnerUserId(member.getUser().getIdLong());
                privateChannel.setUsersWithAccess(Lists.newArrayList());
            }

            privateChannel.setChannelId(c.getIdLong());

            addPermissionsAndMove(c, member,true, privateChannel);
            this.privateChannels.put(member.getUser().getIdLong(), privateChannel);

            try {
                this.save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean hasAccess(Member member, long channelId) {
        PrivateChannel privateChannel = this.getPrivatChannelByChannelId(channelId);

        if(privateChannel == null) {
            return false;
        }

        return privateChannel.getUsersWithAccess().contains(member.getUser().getIdLong());
    }

    public void addMember(long userId, Member toAdd) {
        PrivateChannel privateChannel = this.privateChannels.get(userId);

        ArrayList<Long> users = privateChannel.getUsersWithAccess();

        users.add(toAdd.getUser().getIdLong());

        toAdd.getGuild().getVoiceChannelById(privateChannel.getChannelId()).createPermissionOverride(toAdd).setAllow(Permission.VOICE_CONNECT, Permission.VIEW_CHANNEL).queue(ignored -> {}, Sentry::capture);
    }

    public void removeMember(long userId, Member toAdd) {
        PrivateChannel privateChannel = this.privateChannels.get(userId);

        ArrayList<Long> users = privateChannel.getUsersWithAccess();

        users.remove(toAdd.getUser().getIdLong());

        this.removePermissionsAndKick(toAdd.getGuild().getVoiceChannelById(privateChannel.getChannelId()), toAdd);
    }

    private void load() throws FileNotFoundException {
        this.privateChannels = ThePalaceBot.getInstance().getGson().fromJson(new FileReader(this.privateChannelsFile),
                                                                             new TypeToken<HashMap<Long,
                                                                                     PrivateChannel>>() {}
                                                                                     .getType());
    }

    public void save() throws IOException {
        FileWriter fileWriter = new FileWriter(this.privateChannelsFile);
        fileWriter.write(ThePalaceBot.getInstance().getGson().toJson(this.privateChannels));
        fileWriter.close();
    }

    private void addPermissionsAndMove(Channel channel, Member member, boolean move, PrivateChannel privateChannel) {
        channel.createPermissionOverride(member)
               .setAllow(Permission.VOICE_CONNECT,
                         Permission.VIEW_CHANNEL,
                         Permission.MANAGE_PERMISSIONS,
                         Permission.MANAGE_CHANNEL).queue(success -> {
            if(member.getVoiceState().inVoiceChannel() && move) {
                channel.getGuild().getController().moveVoiceMember(member,
                                                                   (VoiceChannel) channel)
                       .queue(ignored -> {}, Sentry::capture);
            }
        }, Sentry::capture);

        for(Long members : privateChannel.getUsersWithAccess()) {
            Member target = member.getGuild().getMemberById(members);
            if(target == null) return;
            channel.createPermissionOverride(target).setAllow(Permission.VOICE_CONNECT, Permission.VIEW_CHANNEL).queue(ignored -> {}, Sentry::capture);
        }
    }

    private void removePermissionsAndKick(Channel channel, Member member) {
        if(channel.getPermissionOverride(member) == null) return;
        channel.getPermissionOverride(member).delete().queue(ignored -> {
            if(member.getVoiceState().inVoiceChannel() && member.getVoiceState().getChannel().getIdLong() == channel.getIdLong()) {
                VoiceChannelUtils.kick(member);
            }
        });
    }
}
