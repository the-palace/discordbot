package de.nurmarvin.discordbot.handler;

import de.nurmarvin.discordbot.ThePalaceBot;
import io.sentry.Sentry;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ServerStatsHandler extends ListenerAdapter {

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        updateMemberCount(event.getGuild());
    }

    static void updateMemberCount(Guild guild) {
        VoiceChannel voiceChannel =
                guild.getVoiceChannelById(ThePalaceBot.getInstance().getConfig().getMemberCountChannelId());

        voiceChannel.getManager().setName("Member Count: " + guild.getMembers().size()).queue(ignored -> {},
                                                                                              Sentry::capture);
    }
}
