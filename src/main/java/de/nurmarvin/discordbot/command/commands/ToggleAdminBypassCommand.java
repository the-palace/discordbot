package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.Config;
import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.Arrays;

public class ToggleAdminBypassCommand extends Command {
    public ToggleAdminBypassCommand() {
        super("toggleadminbypass", new String[0], "Toggles the ability for bot admins to bypass all filters the bot " +
                                                  "has", false);
    }


    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        Config config = ThePalaceBot.getInstance().getConfig();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        config.setAdminBypass(!config.getAdminBypass());

        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription("Admin Bypass has been " + (config.getAdminBypass() ? "enabled" : "disabled") + ".").build());

        try {
            ThePalaceBot.getInstance().saveConfig();
        } catch (IOException e) {
            Sentry.capture(e);
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
