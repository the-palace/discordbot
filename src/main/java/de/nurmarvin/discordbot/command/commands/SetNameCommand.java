package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

public class SetNameCommand extends Command {
    public SetNameCommand() {
        super("setname", new String[0], "Sets the name of the bot", true);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if(args.length < 1) {
            embedBuilder.appendDescription("Please provide a name.");
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < args.length; i++) {
            if(i != 0)
                stringBuilder.append(" ");
            stringBuilder.append(args[i]);
        }

        msg.getJDA().getSelfUser().getManager().setName(stringBuilder.toString()).queue(
                success -> {
                    embedBuilder.appendDescription("Name updated.");
                    this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
                },
                error -> {
                    Sentry.capture(error);
                    embedBuilder.appendDescription("Error while updating name! `" + error.getMessage() + "`");
                    this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
                });
    }
}
