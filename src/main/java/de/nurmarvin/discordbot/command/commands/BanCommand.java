package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.*;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

import java.time.Instant;

public class BanCommand extends Command {
    public BanCommand() {
        super("ban", new String[0], "Bans a member", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            String reason = stringBuilder.toString();
            long id = MentionHelper.getId(msg.getGuild(), args[0]);

            Member member = msg.getGuild().getMemberById(id);

            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription(
                                               String.format("Banned %s with reason `%s`",
                                                             member == null ? id : member.getAsMention(), reason)).build());

            if(member != null) member.getUser().openPrivateChannel().queue(channel -> {
                EmbedBuilder banMessage = new EmbedBuilder();

                banMessage.setTitle("You have been banned");

                banMessage.addField("Server", msg.getGuild().getName(), false);
                banMessage.addField("Reason", reason, false);
                banMessage.setTimestamp(Instant.now());

                channel.sendMessage(banMessage.build()).queue();

                this.ban(msg.getGuild(), id, reason);
            }, error -> this.ban(msg.getGuild(), id, reason));
            else this.ban(msg.getGuild(), id, reason);
        } else {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription("Please mention the user you " +
                                                                                        "want to ban").build());
        }
    }

    private void ban(Guild guild, long id, String reason) {
        guild.getController().ban(String.valueOf(id), 7, reason).queue();
    }
}