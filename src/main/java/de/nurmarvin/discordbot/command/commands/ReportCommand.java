package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import de.nurmarvin.discordbot.utils.UserInfo;
import de.nurmarvin.discordbot.utils.throwables.InvalidMentionException;
import de.nurmarvin.discordbot.utils.MentionHelper;
import de.nurmarvin.discordbot.utils.throwables.NotAMentionException;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

public class ReportCommand extends Command {

    public ReportCommand() {
        super("report", new String[0], "Report a rule breaker", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();
        UserInfo userInfo = ThePalaceBot.getInstance().getMessageHandler().getUserInfo(msg.getAuthor().getIdLong());

        if(userInfo.getLevel() < 20 && !PermissionHelper.isMod(msg.getMember())) {
            embedBuilder.setTitle("Feature Locked");
            embedBuilder.setDescription("You must be at least **Level 20** to use this feature.");
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
            return;
        }

        if (args.length > 1) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            String reason = stringBuilder.toString();
            Member member = null;
            try {
                member = MentionHelper.getMemberFromMention(msg.getGuild(), args[0]);
            } catch (InvalidMentionException | NotAMentionException e) {
                Sentry.capture(e);
            }

            if(member == null) {
                this.sendAutoDeleteMessage(msg.getChannel(),
                                           embedBuilder.appendDescription("This user was not found on the server").build());
                return;
            }

            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(String.format("Reported %s " +
                                                                                                       "with reason `%s`",
                                                             member.getAsMention(), reason)).build());


            this.report(msg.getGuild(), msg.getMember(), member, reason);
        } else {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription("Please mention the user you " +
                                                                                        "want to report and also " +
                                                                                        "provide a reason").build());
        }
    }

    private void report(Guild guild, Member reporter, Member member, String reason) {
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        embedBuilder.setTitle("Report");

        embedBuilder.addField("Reporter", reporter.getAsMention(), false);
        embedBuilder.addField("Reported User", member.getAsMention(), false);
        embedBuilder.addField("Reason", reason, false);

        TextChannel reportChannel = guild.getTextChannelById(ThePalaceBot.getInstance().getConfig().getReportChannelId());
        reportChannel.sendMessage("@here").queue(m -> reportChannel.sendMessage(embedBuilder.build()).queue());
    }
}
