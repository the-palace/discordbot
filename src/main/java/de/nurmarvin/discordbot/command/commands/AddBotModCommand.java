package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.Arrays;

public class AddBotModCommand extends Command {
    public AddBotModCommand() {
        super("addbotmod", new String[0], "Add a role to be a bot mod", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length == 0) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please add the roles's id as an argument")
                                                   .build());
            return;
        }

        long id;

        try {
            id = Long.parseLong(args[0]);
            if (ThePalaceBot.getInstance().getJda().getRoleById(id) == null) throw new Exception();
        } catch (Exception e) {
            Sentry.capture(e);
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Role ID must be a valid number.").build());
            return;
        }

        if(ThePalaceBot.getInstance().getConfig().getModRoles().contains(id))
        {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("This role is already a bot mod.").build());
            return;
        }

        ThePalaceBot.getInstance().getConfig().getModRoles().add(id);
        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription("Role was added as a bot mod.").build());

        try {
            ThePalaceBot.getInstance().saveConfig();
        } catch (IOException e) {
            Sentry.capture(e);
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
