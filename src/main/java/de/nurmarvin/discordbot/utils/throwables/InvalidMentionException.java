package de.nurmarvin.discordbot.utils.throwables;

public class InvalidMentionException extends Exception {
    public InvalidMentionException() {
        super("The mentioned object doesn't exist");
    }
}
