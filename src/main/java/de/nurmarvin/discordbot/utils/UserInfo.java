package de.nurmarvin.discordbot.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor @NoArgsConstructor @Data
public class UserInfo {
    private Long userId;
    private Long lastMessageSent;
    private Integer level;
    private Long currentXp;
    private Long money;

    public boolean addXP() {
        this.setCurrentXp(this.getCurrentXp() + UtilMath.randomIntBetween(15, 25));
        long reach = this.getXpRequiredForNextLevel();
        if (this.getCurrentXp() >= reach && reach != 0) {
            this.setCurrentXp(0L);
            this.setLevel(getLevel() + 1);
            return true;
        }
        return false;
    }

    public long getXpRequiredForNextLevel() {
        return (long) Math.floor(5 * (Math.pow(this.getLevel(), 2)) + 50 * this.getLevel() + 100);
    }
}
