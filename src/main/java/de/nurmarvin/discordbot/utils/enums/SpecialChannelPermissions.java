package de.nurmarvin.discordbot.utils.enums;

public enum SpecialChannelPermissions {
    IMAGES,
    LINKS,
    BOT_COMMANDS,
    SPAM;

    public static SpecialChannelPermissions get(String s) {
        return valueOf(s.toUpperCase());
    }
}
